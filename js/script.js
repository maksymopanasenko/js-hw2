let userName;
let userAge;

do {
    userName = prompt('What is your name?', [userName]);
    userAge = +prompt('How old are you?', [!isNaN(userAge) ? userAge : null]); // prevent to show 'NaN' in the input, while a name wasn`t entered before
} while (!userName || !Number.isInteger(userAge));

if (userAge < 18) {
    alert('You are not allowed to visit this website');
} else if (userAge >= 18 && userAge <= 22) {
    let answer = confirm('Are you sure you want to continue?');
    answer ? alert(`Welcome, ${userName}`) : alert('You are not allowed to visit this website');
} else {
    alert(`Welcome, ${userName}`);
}